<?php
    // get-data
    $filename = $folderName . '/' . str_replace("..", ".", str_replace(".zip", "", strip_tags($_POST['file_title']))) . '.zip';
    $files = explode("\n", strip_tags($_POST['links']));

    // create folder
    $folderName = date("YmdHis");
    mkdir($folderName, 0777, true);

    // $files = array('https://stored.ant.lgbt/img/icon_225.png');

    // create zip
    $zip = new ZipArchive();

    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
        exit("cannot open '$filename'\n");
    } else {
        echo "Created ZipFile '$filename'<br>";
    }

    foreach ($files as $file) {
        // download images
        echo "Downloading file: '$file' to '" . $folderName . '/' . basename($file) . "'and adding it to the zip file<br>";
        file_put_contents($folderName . '/' . basename($file), fopen($file, 'r'));
        // add to file
        $zip->addFile($folderName . '/' . basename($file), basename($file));
        echo "numfiles: " . $zip->numFiles . "\n";
        echo "<br>status:" . $zip->status . "\n";
    }
    $zip->close();

    ?>